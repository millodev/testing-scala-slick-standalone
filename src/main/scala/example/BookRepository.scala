package example

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

import slick.basic.DatabaseConfig
import slick.driver.JdbcProfile
import slick.jdbc.MySQLProfile.api._
import scala.concurrent.Future
import scala.util.Success
import scala.util.Failure
import Thread.sleep

object BookRepository {

  val books = TableQuery[BookTable]

  val db = Database.forConfig("mysql_dc")

  def main(args: Array[String]): Unit = {

    val newBook = Book(
      0,
      "Twilight",
      "Stephenie Meyer",
      "Little, Brown Books for Young Readers",
      544,
      "978-0316015844"
    )

    val newBooks: Seq[Book] = Seq(
      Book(
        0,
        "Java: The Complete Reference, Eleventh Edition 11th Edition",
        "Herbert Schildt",
        "Oracle",
        1248,
        "978-1260440232"
      ),
      Book(
        0,
        "Head First Java, 2nd Edition 2nd Edition",
        "Kathy Sierra",
        "O Reilly",
        688,
        "978-0596009205"
      ),
      Book(
        0,
        "Effective Java 3rd Edition",
        "Joshua Bloch",
        "Pearson",
        416,
        "978-0134685991"
      ),
      Book(
        0,
        "Programming in Scala",
        "Martin Odersky",
        "Artima Press",
        668,
        "978-0997148008"
      ),
      Book(
        0,
        "Scala Cookbook: Recipes for Object-Oriented and Functional Programming 2nd Edition",
        "Alvin Alexander",
        "O Reilly",
        802,
        "978-1492051541"
      ),
      Book(
        0,
        "Hands-on Scala Programming: Learn Scala in a Practical, Project-Based Way",
        "Haoyi Li",
        "Li Haoyi",
        414,
        "978-9811456930"
      ),
      Book(
        0,
        "JavaScript: The Definitive Guide: Master the World's Most-Used Programming Language 7th Edition",
        "David Flanagan",
        "O Reilly",
        706,
        "978-1491952023"
      )
    )

    /** single creation
      */
    val id: Long = Await.result(create(newBook), Duration.Inf)

    /** bulk creation
      */
    Await.result(createAll(newBooks), Duration.Inf).map(b => println(b))

    /** find by like keyword
      */
    Await.result(findByTitle("%Java%"), Duration.Inf).map(b => println(b))

    /** find by Isbn
      */
    println(Await.result(findByIsbn("978-1492051541"), Duration.Inf));

    /** removing a record.
      */
    println(Await.result(remove(id), Duration.Inf))

    /** updating a record.
      */

    update("978-0997148008", "Programming in Scala Fifth Edition")

  }

  def update(isbn: String, title: String): Future[Int] = {
    val q = for { b <- books if b.isbn === isbn } yield b.title
    db.run(q.update(title))
  }

  def remove(id: Long): Future[Int] = {
    db.run(books.filter(_.id === id).delete)
  }

  def findByIsbn(isbn: String): Future[Book] = {
    db.run(books.filter(_.isbn === isbn).result.head)
  }

  def create(book: Book): Future[Long] = {
    db.run { books returning books.map(_.id) += book }
  }

  def createAll(newBooks: Seq[Book]): Future[Option[Int]] = {
    db.run(books ++= newBooks)
  }

  def findByTitle(title: String): Future[Seq[Book]] = {
    db.run(books.filter(_.title like title).result)
  }

  case class Book(
      id: Long,
      title: String,
      author: String,
      publisher: String,
      pages: Int,
      isbn: String
  )

  class BookTable(tag: Tag) extends Table[Book](tag, "book") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def title = column[String]("title")
    def author = column[String]("author")
    def publisher = column[String]("publisher")
    def pages = column[Int]("pages")
    def isbn = column[String]("isbn")
    def * = (id, title, author, publisher, pages, isbn).mapTo[Book]
  }
}
