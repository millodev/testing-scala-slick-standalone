import Dependencies._

ThisBuild / scalaVersion := "2.13.6"
ThisBuild / version := "0.1.0-SNAPSHOT"
ThisBuild / organization := "com.example"
ThisBuild / organizationName := "example"

lazy val root = (project in file("."))
  .settings(
    name := "testing-scala-slick-standalone",
    libraryDependencies += scalaTest % Test,
    libraryDependencies += "com.typesafe.slick" %% "slick" % "3.3.2",
    libraryDependencies += "com.typesafe.slick" %% "slick-hikaricp" % "3.3.2",
    libraryDependencies += "mysql" % "mysql-connector-java" % "8.0.23"
  )
