**testing-scala-slick.standalone**

Show a little examples of CRUD using Slick and MySql.

Mininal version:

- Scala 2.13.6
- Sbt 1.5.5
- Slick 3.3.2


Table Book for this example

```
+-----------+------------------+------+-----+---------+----------------+
| Field     | Type             | Null | Key | Default | Extra          |
+-----------+------------------+------+-----+---------+----------------+
| id        | int(10) unsigned | NO   | PRI | NULL    | auto_increment |
| title     | varchar(228)     | NO   |     | NULL    |                |
| author    | varchar(64)      | YES  |     | NULL    |                |
| publisher | varchar(64)      | YES  |     | NULL    |                |
| pages     | smallint(6)      | YES  |     | NULL    |                |
| isbn      | varchar(16)      | YES  |     | NULL    |                |
+-----------+------------------+------+-----+---------+----------------+
6 rows in set (0.001 sec)
```



The script for create the schema.

```

-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema testing-scala-slick-standalone-db
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `testing-scala-slick-standalone-db` ;

-- -----------------------------------------------------
-- Schema testing-scala-slick-standalone-db
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `testing-scala-slick-standalone-db` DEFAULT CHARACTER SET utf8 ;
SHOW WARNINGS;
USE `testing-scala-slick-standalone-db` ;

-- -----------------------------------------------------
-- Table `testing-scala-slick-standalone-db`.`book`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `testing-scala-slick-standalone-db`.`book` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `testing-scala-slick-standalone-db`.`book` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(228) NOT NULL,
  `author` VARCHAR(64) NULL,
  `publisher` VARCHAR(64) NULL,
  `pages` SMALLINT NULL,
  `isbn` VARCHAR(16) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

SHOW WARNINGS;
CREATE UNIQUE INDEX `id_UNIQUE` ON `testing-scala-slick-standalone-db`.`book` (`id` ASC) VISIBLE;

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
```
